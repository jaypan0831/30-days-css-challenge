import React, { Suspense } from 'react';
import styled from 'styled-components';
import { Controller, Scene } from 'react-scrollmagic';
import { Tween, Timeline } from 'react-gsap';
import { useImage } from 'react-image';

import Banner1 from './images/banner-1.jpg';
import Banner2 from './images/banner-2.jpg';

const SectionWipes2Styled = styled.div`
  img {
    width: 100%;
    height: 100%;
  }

  overflow: hidden;
  #pinContainer {
    height: 100vh;
    width: 100vw;
    overflow: hidden;
  }
  #pinContainer .panel {
    height: 100vh;
    width: 100vw;
    position: absolute;
    text-align: center;
  }
  .panel span {
    position: relative;
    display: block;
    top: 50%;
    font-size: 80px;
  }

  .panel.blue {
    background-color: #3883d8;
  }

  .panel.turqoise {
    background-color: #38ced7;
  }

  .panel.green {
    background-color: #22d659;
  }

  .panel.bordeaux {
    background-color: #953543;
  }
`;

const Image = ({ url }) => {
  const { src } = useImage({ srcList: url });

  return <img src={src} />;
};

const App = () => (
  <SectionWipes2Styled>
    <Controller>
      <Scene triggerHook="onLeave" duration="150%" pin>
        <Timeline wrapper={<div id="pinContainer" />}>
          <section className="panel blue">
            <Suspense fallback="...loading">
              <Image url={Banner1} />
            </Suspense>
          </section>
          <Tween from={{ y: '-100%' }} to={{ y: '0%' }}>
            <section className="panel turqoise">
              <Suspense fallback="...loading">
                <Image url={Banner2} />
              </Suspense>
            </section>
          </Tween>
          <Tween from={{ x: '100%' }} to={{ x: '0%' }}>
            <section className="panel green">
              <Suspense fallback="...loading">
                <Image url={Banner1} />
              </Suspense>
            </section>
          </Tween>
          <Tween from={{ y: '-100%' }} to={{ y: '0%' }}>
            <section className="panel bordeaux">
              <Suspense fallback="...loading">
                <Image url={Banner2} />
              </Suspense>
            </section>
          </Tween>
        </Timeline>
      </Scene>
    </Controller>
  </SectionWipes2Styled>
);

export default App;
